#!/usr/bin/env python3

"""A demo client for Open Pixel Control
http://github.com/zestyping/openpixelcontrol

Creates a shifting rainbow plaid pattern by overlaying different sine waves
in the red, green, and blue channels.

This version sends one frame's worth of pixels, then exits.  It also runs
at 4% of normal speed.

To run:
First start the gl simulator using the included "wall" layout

    make
    bin/gl_server layouts/wall.json

Then run this script in another shell to send colors to the simulator

    python_clients/raver_plaid.py

"""


import time
import math
import sys

import opc
import color_utils


#-------------------------------------------------------------------------------
# handle command line

if len(sys.argv) == 1:
    IP_PORT = '127.0.0.1:7890'
elif len(sys.argv) == 2 and ':' in sys.argv[1] and not sys.argv[1].startswith('-'):
    IP_PORT = sys.argv[1]
else:
    print()
    print('    Usage: raver_plaid.py [ip:port]')
    print()
    print('    If not set, ip:port defauls to 127.0.0.1:7890')
    print()
    sys.exit(0)


#-------------------------------------------------------------------------------
# connect to server

client = opc.Client(IP_PORT)
if client.can_connect():
    print('    connected to %s' % IP_PORT)
else:
    # can't connect, but keep running in case the server appears later
    print('    WARNING: could not connect to %s' % IP_PORT)
print()


#-------------------------------------------------------------------------------
# send pixels

print('    sending pixels forever (control-c to exit)...')
print()

n_pixels = 300  # number of pixels in the included "wall" layout
fps = 20         # frames per second

# how many sine wave cycles are squeezed into our n_pixels
# 24 happens to create nice diagonal stripes on the wall layout
freq_r = 24
freq_g = 24
freq_b = 24

# how many seconds the color sine waves take to shift through a complete cycle
speed_r = 7
speed_g = -13
speed_b = 19

t = time.time() - 1370000000
t *= 0.04 # run at 4% of normal speed
pixels = []
for ii in range(n_pixels):
    pixels.append((0, 0, 0))
client.put_pixels(pixels, channel=0)

time.sleep(1)
client.put_pixels(pixels, channel=0)
